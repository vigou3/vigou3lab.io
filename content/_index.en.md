---
title: Vincent Goulet
description: Professor of Actuarial Science. Closet geek.
---

This page lists my [open educational resources](https://en.wikipedia.org/wiki/Open_educational_resources), software, conferences and workshops. The material is published under a [free license](https://en.wikipedia.org/wiki/Free_license). The source code of these projects is hosted on [GitLab](https://gitlab.com/vigou3).

# Open educational resources

## R and programming

[**Programmer avec R**](https://vigou3.gitlab.io/programmer-avec-r/)
Introduction to programming textbook (in French) using [R](https://www.r-project.org) as programming language.

[**Présentation de R &mdash; Code réel, objets virtuels**](https://vigou3.gitlab.io/presentation-r/)
Short training course (in French) on first steps with R and the work strategy known as [*the source code is real*](http://ess.r-project.org/Manual/ess.html#Philosophies-for-using-ESS_0028R_0029).

[**Conception de paquetages R**](https://vigou3.gitlab.io/conception-paquetages-r)
Introductory training course (in French) on building [R](https://www.r-project.org) packages.

[**Programmation lettrée et R Markdown**](https://vigou3.gitlab.io/programmation-lettree-et-rmarkdown)
Introductory training course (in French) on literate programming in general and [R Markdown](https://rmarkdown.rstudio.com) in particular.

[**Rapports dynamiques avec Shiny**](https://vigou3.gitlab.io/rapports-dynamiques-avec-shiny)
Introductory training course (in French) on production of dynamic reports with [Shiny](https://shiny.rstudio.com).

[**Utilisation et conception d'interfaces API**](https://vigou3.gitlab.io/utilisation-et-conception-interfaces-api)
Introductory training course (in French) on APIS: accessing them from the Unix command line with curl, and creating one in R with [**plumber**](https://rplumber.io).

[**Ligne de commande Unix**](https://vigou3.gitlab.io/ligne-commande-unix/)
Introductory training course (in French) on the Unix command line interface (notably [Bash](https://www.gnu.org/software/bash/)).

[**Gestion de versions avec Git**](https://vigou3.gitlab.io/gestion-versions-avec-git/)
Introductory training course (in French) on version control with [Git](https://git-scm.com).
  
[**Calcul numériques de primes bayésiennes avec Stan**](https://vigou3.gitlab.io/calcul-primes-bayesiennes-avec-stan/) 
Introductory training course (in French) on the computation of bayesian premiums in the context of credibility theory using the platform for statistical modeling [Stan](https://mc-stan.org/). 

## Actuarial science

[**Méthodes numériques en actuariat avec R**](https://vigou3.gitlab.io/methodes-numeriques-en-actuariat-avec-r/)
Reference manual (in French) covering random number generation, numerical analysis and linear algebra. The numerical aspects are treated with [R](https://www.r-project.org).

[**Modélisation des distributions de sinistres avec R**](https://vigou3.gitlab.io/modelisation-distributions-sinistres-avec-r/)
Exhaustive reference manual (in French) on statistical modeling of loss distributions using [R](https://www.r-project.org) and package [**actuar**](https://cran.r-project.org/package=actuar).

[**Théorie de la crédibilité avec R**](https://vigou3.gitlab.io/theorie-credibilite-avec-r/)
Reference manual (in French) on credibility theory. Covers limited fluctuation credibility, Bayesian ratemaking and the Bühlmann and Bühlmann-Straub credibility models. Numerical illustrations are done with [R](https://www.r-project.org) and package [**actuar**](https://cran.r-project.org/package=actuar).

[**Analyse statistique --- Exercices et solutions**](https://vigou3.gitlab.io/analyse-statistique-exercices-et-solutions/)
Exercise manual for a mathematical statistics course of undergraduate or graduate level.

## LaTeX

[**Rédaction avec LaTeX**](https://vigou3.gitlab.io/formation-latex-ul)
Introductory course (in French) on the LaTeX typesetting system. Also released [on CTAN](https://ctan.org/pkg/formation-latex-ul).

[**bibliography**](https://gitlab.com/vigou3/bibliography)
My bibliography databases in BIBTeX format. Having them under version control is a Good Thing. 


# Software

## Distributions of GNU Emacs

[**Emacs modified for (macOS|Windows)**](https://emacs-modified.gitlab.io)
Distributions of [GNU Emacs](https://www.gnu.org/software/emacs/) bundled with a few select packages for LaTeX users and R developers, most notably [AUCTeX](https://www.gnu.org/software/auctex/) and [ESS](https://ess.r-project.org/). The Windows version ships with an installation wizard.

## Roger the Omni Grader

[**Roger Project**](https://roger-project.gitlab.io/en) 
Roger is an automated grading system for computer programming projects. Although specialized for R scripts, Roger can easily be adapted to grade code in other interpreted or compiled programming languages, thanks to its flexibility and modularity.

## R packages

[**actuar**](https://gitlab.com/vigou3/actuar)
First R package dedicated specifically to actuarial computations and modeling. Official releases [on CRAN](https://cran.r-project.org/package=actuar).

[**expint**](https://gitlab.com/vigou3/expint)
Small R package to compute the exponential integral and the incomplete gamma function. Official releases [on CRAN](https://cran.r-project.org/package=expint).

[**RweaveExtra**](https://gitlab.com/vigou3/RweaveExtra) 
Small R package that provides the drivers `RweaveExtraLatex` and `RtangleExtra` for the Sweave literate programming system. These extend the standard drivers `RweaveLatex` and `Rtangle`, respectively. The drivers provide additional options to completely ignore code chunks on weaving, tangling, or both. The driver `RtangleExtra` also provides an option `extension` to specify the extension of the file name (or remove it entirely) when splitting is selected on tangling. Official releases [on CRAN](https://cran.r-project.org/package=RweaveExtra).

## LaTeX packages

[**ulthese**](https://vigou3.gitlab.io/ulthese)
Document class for theses and memoirs at [Université Laval](https://ulaval.ca). Official release [on CTAN](https://ctan.org/pkg/ulthese).

[**francais-bst**](https://gitlab.com/vigou3/francais-bst)
Natbib compatible bibliography style files to typeset bibliographies using the French typographic standards. Official release [on CTAN](https://ctan.org/pkg/francais-bst).

[**actuarialsymbol**](https://vigou3.gitlab.io/actuarialsymbol)
Commands to compose actuarial symbols of life contingencies and financial mathematics characterized by subscripts and superscripts on both sides of a principal symbol. Official release [on CTAN](https://ctan.org/pkg/actuarialsymbol).

[**actuarialangle**](https://vigou3.gitlab.io/actuarialsymbol)
Commands to typeset the *angle* symbol denoting a duration in actuarial notation, such as in symbols for the present value of certain or life annuities, and an over angle bracket used to emphasize joint status in symbols of life contingencies. Official release [on CTAN](https://ctan.org/pkg/actuarialangle).
  
I'm also the developer and maintainer of the package [**cjs-rcs-article**](https://cjs-rcs.gitlab.io/fr) for [The Canadian Journal of Statistics](https://ssc.ca/en/publications/canadian-journal-statistics).


# Outliers

[**Uniform BIXI Open Data**](https://vigou3.gitlab.io/bixi-donnees-ouvertes-uniformes/en)
The non-profit organization [BIXI Montréal](https://www.bixi.com/en) publishes trip history and station status open data of its bike-sharing system. However, the format of this data changes from one year to another. This projects provides the original BIXI open data in a unique, uniform format.

[**BitBucketAPI**](https://gitlab.com/vigou3/bitbucketapi)
Unix shell scripts to achieve select administrative operations on a BitBucket repository using the Atlassian REST API.


# Conference and workshop material

[**Bernoulli, Bayes et la protection contre
 l'infortune**](https://gitlab.com/vigou3/bernoulli-bayes-et-vous)
Slides of a conference (in French) introducing the basic concepts of insurance and actuarial science. 

[**IA et reconnaissance de caractères - Atelier d'introduction à la sciences des données**](https://vigou3.gitlab.io/atelier-science-donnees/)
Material of a workshop (in French) providing an introduction to data science in the context of optical character recognition. Includes additional handwritten digits to compare with the famous [Optical Recognition of Handwritten Digits](https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits) dataset. Also contains a companion [Shiny app](https://vigou3.shinyapps.io/atelier-science-donnees).

[**Gérer ses documents efficacement avec la programmation lettrée - R à Québec 2019**](https://gitlab.com/vigou3/raquebec-programmation-lettree)
Slides of a conference (in French) presented at [R à Québec 2019](http://raquebec.ulaval.ca/2019/) showcasing how using the *tangle* step of literate programming in addition to the well known *weave* step can improve administration as well as consistency of one's documents.

[**Programmer pour collaborer: utilisation et conception d'une interface de programmation applicative (API) - R à Montréal 2018**](https://gitlab.com/vigou3/ramontreal-expint)
Slides of a conference (in French) presented at [R à Montréal 2018](http://rmontreal2018.ca/) explaining how to provide a C API in an R package and link to it from another package.

[**You (S)wove? Well (S)tangle now!**](https://gitlab.com/vigou3/tug-2024-you-swove-well-stangle-now)
Slides of a talk presented at [TUG 2024](https://tug.org/tug2024/), not unlike the R à Québec 2019 one, above, but with a refreshed presentation that does not rely on external source code.

[**A journey through the design of (yet another) journal class**](https://gitlab.com/vigou3/tug-2024-cjs-rcs-article)
Slides of a talk at [TUG 2024](https://tug.org/tug2024/) outlining the development of the package [**cjs-rcs-article**](https://cjs-rcs.gitlab.io) providing the LaTeX class and bibliographic styles for articles in [*The Canadian Journal of Statistics*](https://ssc.ca/en/publications/canadian-journal-statistics).

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
