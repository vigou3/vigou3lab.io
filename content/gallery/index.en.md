---
title: Cover pages gallery
---

Actuarial science is not a discipline that is easy to illustrate
(beyond the obvious umbrella and monetary symbol). Inspired by the
famous computer science 
[book covers](https://www.oreilly.com/animals.csp)
of O'Reilly, I opted a number of years ago to use pictures of animals
on the covers of my documents. Obtained from 
[Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page), 
all images are released under libre
[Creative Commons licenses](https://en.wikipedia.org/wiki/Creative_Commons). 
Many are
[featured pictures](https://commons.wikimedia.org/wiki/Commons:Featured_pictures)
or
[valued images](https://commons.wikimedia.org/wiki/Commons:Valued_images).

But for a few exceptions, none have a link to the subject of the document.

## Reference manuals

{{< gallery-portrait >}} 

## Conference slides

{{< gallery-landscape >}} 
