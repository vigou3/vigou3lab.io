---
title: Galerie de couvertures
---

L'actuariat est une discipline difficile à illustrer (au-delà des
sempiternels parapluies et symboles monétaires). Inspiré par les
[célèbres couvertures](https://www.oreilly.com/animals.csp)
des livres d'informatique de O'Reilly, j'ai donc
décidé, il y a maintenant plusieurs années, d'illustrer la plupart de
mes documents par une photo d'animal. Les images tirées de 
[Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page?uselang=fr) 
sont toutes publiées sous des 
[licences libres Creative Commons](https://fr.wikipedia.org/wiki/Creative_Commons). 
Plusieurs font partie des collections
d'[images remarquables](https://commons.wikimedia.org/wiki/Commons:Featured_pictures/fr)
ou
d'[images de valeur](https://commons.wikimedia.org/wiki/Commons:Valued_images/fr).

Sauf de rares exceptions, aucune n'a de lien avec le sujet de
l'ouvrage.

## Documents de référence

{{< gallery-portrait >}} 

## Diapositives de présentations

{{< gallery-landscape >}} 
